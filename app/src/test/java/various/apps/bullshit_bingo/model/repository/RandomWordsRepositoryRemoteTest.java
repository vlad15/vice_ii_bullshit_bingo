package various.apps.bullshit_bingo.model.repository;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class RandomWordsRepositoryRemoteTest {

    RandomWordsRepository randomWordsRepository;

    @Before
    public void setup() {

        randomWordsRepository = new RandomWordsRepositoryRemote();
    }

    @Test
    public void get() throws Exception {
        String someWord = randomWordsRepository.get().toBlocking().first();

        assertNotNull(someWord);
        assertTrue(someWord.length() > 0);
    }


    @Test
    public void getMultiple() throws Exception {

        final int WORDS_QUANTITY = 5;

        List<String> words = randomWordsRepository.get(WORDS_QUANTITY).toBlocking().first();

        assertNotNull(words);
        assertEquals(WORDS_QUANTITY, words.size());
    }

}