package various.apps.bullshit_bingo.model.interactor;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

import rx.Observable;
import various.apps.bullshit_bingo.model.domain.ImageWithTitle;
import various.apps.bullshit_bingo.model.repository.ImageUrlRepository;
import various.apps.bullshit_bingo.model.repository.RandomWordsRepository;

public class GetPagedImagesWithTitlesMurrayInteractorTest {

    GetImagesWithTitlesMurrayInteractor getImagesWithTitlesMurrayInteractor;
    PredefinedWordsRepository predefinedWordsRepository;

    // Testing both full and not full page
    private final int PER_PAGE = 2;

    private TestCase[] TEST_DATA = {
        new TestCase("Image1", "Url1"),
        new TestCase("Image2", "Url2"),
        new TestCase("Image3", "Url3")
    };

    class TestCase {
        private String title;
        private String url;

        public TestCase(String title, String url) {
            this.title = title;
            this.url = url;
        }

        public String getTitle() {
            return title;
        }

        public String getUrl() {
            return url;
        }

    }

    @Before
    public void setup() {
        ImageUrlRepository imageUrlRepository = (page, perPage) -> {

            List<String> urls = new ArrayList<>();

            int firstElement = perPage * page;

            for (int i = firstElement;
                     i < Math.min(TEST_DATA.length, firstElement + perPage); ++i) {
                urls.add(TEST_DATA[i].getUrl());
            }
            return Observable.just(urls);

        };
        predefinedWordsRepository = new PredefinedWordsRepository();

        getImagesWithTitlesMurrayInteractor =
                new GetImagesWithTitlesMurrayInteractor(
                        imageUrlRepository, predefinedWordsRepository
                );
    }

    @Test
    public void get() throws Exception {

        int currentCase = 0;
        int totalResults = 0;
        for (int page = 0; page < TEST_DATA.length / PER_PAGE + 1; ++page) {

            predefinedWordsRepository.setOffset(totalResults);

            List<ImageWithTitle> results =
                    getImagesWithTitlesMurrayInteractor.get(page, PER_PAGE)
                            .toBlocking().first();

            assertNotNull(results);

            assertTrue(results.size() <= PER_PAGE);

            for (int i = 0; i < results.size(); ++i) {
                TestCase testCase = TEST_DATA[currentCase];
                ImageWithTitle result = results.get(i);

                assertEquals(testCase.getTitle(), result.getTitle());
                assertEquals(testCase.getUrl(), result.getImageUrl());

                currentCase += 1;
            }

            totalResults += results.size();
        }

        assertEquals(TEST_DATA.length, totalResults);
    }

    private class PredefinedWordsRepository implements RandomWordsRepository {

        int offset = 0;

        protected void setOffset(int offset) {
            this.offset = offset;
        }

        @Override
        public Observable<String> get() {
            return get(1).flatMap(Observable::from).first();
        }

        @Override
        public Observable<List<String>> get(int quantity) {
            List<String> titles = new ArrayList<>();
            for (int i = 0; i < quantity; ++i) {
                titles.add(TEST_DATA[(offset + i) % TEST_DATA.length].getTitle());
            }
            return Observable.just(titles);
        }
    }
}