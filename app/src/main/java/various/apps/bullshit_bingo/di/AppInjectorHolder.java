package various.apps.bullshit_bingo.di;

/**
 * Contains AppInjector.
 * Supposed to be Android application component.
 */
public interface AppInjectorHolder {
    AppInjector getAppInjector();
}
