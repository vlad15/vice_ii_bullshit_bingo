package various.apps.bullshit_bingo.di.Scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Scope: View plus SoftReference
 * May be reused in next View instance
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ViewSoftScope {
}
