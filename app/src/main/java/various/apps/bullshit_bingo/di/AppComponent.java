package various.apps.bullshit_bingo.di;

import android.content.Context;

import dagger.Component;

/**
 * Application component
 *
 * Provides application context
 */

@Component(modules = {AppModule.class})
public interface AppComponent {
    Context context();
}
