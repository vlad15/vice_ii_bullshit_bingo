package various.apps.bullshit_bingo.di;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    Context appContext;

    public AppModule(Context appContext) {
        this.appContext = appContext;
    }

    @Provides
    public Context provideAppContext() {
        return appContext;
    }
}
