package various.apps.bullshit_bingo.di;

import various.apps.bullshit_bingo.view.DetailsActivity;
import various.apps.bullshit_bingo.view.MainActivity;


/**
 * Interface with injection methods
 *
 * Supposed usage:
 * Main component and test component extend this interface.
 * When something needs to be injected, it needs to be done using this interface
 * instead of concrete component. This enables modules substitution for tests.
 *
 *
 * option 1:
 * void inject(Target whereToInject);
 *
 * option 2:
 * Dependency getSomeDependency(); // method with any name
 */
public interface AppInjector {
    void inject(MainActivity mainActivity);
    void inject(DetailsActivity detailsActivity);
}
