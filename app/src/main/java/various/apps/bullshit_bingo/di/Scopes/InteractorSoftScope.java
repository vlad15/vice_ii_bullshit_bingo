package various.apps.bullshit_bingo.di.Scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Scope: Interactor plus SoftReference
 * May be reused in other interactor instance.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface InteractorSoftScope {
}
