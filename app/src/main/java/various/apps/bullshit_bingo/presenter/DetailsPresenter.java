package various.apps.bullshit_bingo.presenter;

import various.apps.base.presenter.Presenter;
import various.apps.bullshit_bingo.view.DetailsView;

public interface DetailsPresenter extends Presenter<DetailsView> {
}
