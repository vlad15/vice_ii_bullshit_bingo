package various.apps.bullshit_bingo.presenter;

import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func0;
import rx.schedulers.Schedulers;
import various.apps.base.presenter.loaders.PresenterWithLoaders;
import various.apps.base.presenter.loaders.SerialRxLoaderWithMemory;
import various.apps.base.view.DataReceiver;
import various.apps.bullshit_bingo.model.domain.ImageWithTitle;
import various.apps.bullshit_bingo.model.domain.PagingPosition;
import various.apps.bullshit_bingo.model.interactor.GetImagesWithTitlesInteractor;
import various.apps.bullshit_bingo.view.MainView;


public class MainPresenterImpl extends PresenterWithLoaders<MainView> implements MainPresenter {

    GetImagesWithTitlesInteractor getImagesWithTitlesInteractor;

    ImagesWithTitlesLoader imagesWithTitlesLoader = addLoader(new ImagesWithTitlesLoader(
            getDataReceiverIfViewAttached(() -> this.view.getImagesWithTitlesReceiver()),
            true, false
    ) {
          @Override
          protected void saveDataForNextReceiver(List<ImageWithTitle> imageWithTitles, PagingPosition pagingPosition) {
              if (dataForNextReceiver == null) {
                  dataForNextReceiver = new ArrayList<>();
              }

              // If we received first page, it means that screen is refreshed.
              // So data for the next view needs to contain only the new data.
              if (pagingPosition.getPage() == 0) {
                  dataForNextReceiver.clear();
              }

              dataForNextReceiver.addAll(imageWithTitles);
          }
      }
    );

    public MainPresenterImpl(GetImagesWithTitlesInteractor getImagesWithTitlesInteractor) {
        this.getImagesWithTitlesInteractor = getImagesWithTitlesInteractor;
    }

    @Override
    public void onOpenForFirstTime() {
        if (!imagesWithTitlesLoader.gotDataForNextReceiver()) {
            imagesWithTitlesLoader.loadPage(
                    new PagingPosition(0, view.getItemsPerPage())
            );
        }
    }

    @Override
    public void onSwipedToRefresh() {
        imagesWithTitlesLoader.loadPage(
                new PagingPosition(0, view.getItemsPerPage())
        );
    }

    @Override
    public void onLastElementVisible() {
        if (!imagesWithTitlesLoader.isLoading()) {
            imagesWithTitlesLoader.loadPage(
                    new PagingPosition(view.getPagesLoad(), view.getItemsPerPage())
            );
        }
    }

    class ImagesWithTitlesLoader extends
            SerialRxLoaderWithMemory<List<ImageWithTitle>, PagingPosition> {

        public ImagesWithTitlesLoader(
                Func0<DataReceiver<List<ImageWithTitle>, PagingPosition>> getDataReceiver,
                boolean displayDataToNextReceiversToo,
                boolean displayErrorToNextReceiversToo) {

            super(getDataReceiver, displayDataToNextReceiversToo, displayErrorToNextReceiversToo);
        }

        private void loadPage(PagingPosition pagingPosition) {
            subscribeToResult(
                    getImagesWithTitlesInteractor.get(
                            pagingPosition.getPage(),
                            pagingPosition.getItemsPerPage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()),
                    pagingPosition);
        }
    }
}
