package various.apps.bullshit_bingo.presenter;

import android.os.Bundle;
import android.support.annotation.Nullable;

import various.apps.base.presenter.BasePresenter;
import various.apps.bullshit_bingo.view.DetailsView;

public class DetailsPresenterImpl extends BasePresenter<DetailsView> implements DetailsPresenter {
    @Override
    public void onCreate(@Nullable Bundle arguments, @Nullable Bundle savedInstanceState) {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {

    }

    @Override
    public void onDestroy() {

    }
}
