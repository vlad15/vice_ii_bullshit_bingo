package various.apps.bullshit_bingo.presenter.di;


import dagger.Module;
import dagger.Provides;
import various.apps.bullshit_bingo.di.Scopes.ViewSoftScope;
import various.apps.bullshit_bingo.model.interactor.GetImagesWithTitlesInteractor;
import various.apps.bullshit_bingo.presenter.DetailsPresenter;
import various.apps.bullshit_bingo.presenter.DetailsPresenterImpl;
import various.apps.bullshit_bingo.presenter.MainPresenter;
import various.apps.bullshit_bingo.presenter.MainPresenterImpl;

@Module
public class PresentersModule {

    @Provides
    @ViewSoftScope
    public MainPresenter provideMainPresenter(
            GetImagesWithTitlesInteractor getImagesWithTitlesInteractor) {
        return new MainPresenterImpl(getImagesWithTitlesInteractor);
    }

    @Provides
    public DetailsPresenter provideDetailsPresenter() {
        return new DetailsPresenterImpl();
    }
}