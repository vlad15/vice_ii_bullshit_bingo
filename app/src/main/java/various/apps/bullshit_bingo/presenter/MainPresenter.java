package various.apps.bullshit_bingo.presenter;

import various.apps.base.presenter.Presenter;
import various.apps.bullshit_bingo.view.MainView;

public interface MainPresenter extends Presenter<MainView> {
    void onOpenForFirstTime();

    void onSwipedToRefresh();

    void onLastElementVisible();
}
