package various.apps.bullshit_bingo.presenter.di;

import dagger.Component;
import various.apps.bullshit_bingo.di.AppComponent;
import various.apps.bullshit_bingo.di.AppInjector;
import various.apps.bullshit_bingo.di.Scopes.ViewSoftScope;
import various.apps.bullshit_bingo.model.interactor.di.InteractorsComponent;
import various.apps.bullshit_bingo.presenter.DetailsPresenter;
import various.apps.bullshit_bingo.presenter.MainPresenter;

/**
 * Root element for dependencies hierarchy, can be used to inject all dependencies
 */

@ViewSoftScope
@Component(
        dependencies = {
                AppComponent.class,
                InteractorsComponent.class
        },
        modules = {PresentersModule.class}
)
public interface PresentersComponent extends AppInjector {
    MainPresenter mainPresenter();
    DetailsPresenter detailsPresenter();
}
