package various.apps.bullshit_bingo.view.adapters;

import android.content.Context;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action3;
import various.apps.bullshit_bingo.R;
import various.apps.bullshit_bingo.model.domain.ImageWithTitle;

public class ImageWithTitleAdapter extends
        RecyclerView.Adapter<ImageWithTitleAdapter.ImageWithTitleViewHolder> {

    private List<ImageWithTitle> elements = new ArrayList<>();
    private Action3<Pair<View, String>[],
            String,
            String> onElementClicked;

    Context context;
    LayoutInflater inflater;

    public ImageWithTitleAdapter(Context context,
                                 Action3<Pair<View, String>[],
                                         String,
                                         String> onElementClicked) {
        this.context = context;
        this.onElementClicked = onElementClicked;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ImageWithTitleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = inflater.inflate(R.layout.item_image_with_title, parent, false);

        return new ImageWithTitleViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ImageWithTitleViewHolder holder, int position) {

        ImageWithTitle imageWithTitle = elements.get(position);

        holder.title.setText(imageWithTitle.getTitle());

        holder.imageProgress.setVisibility(View.VISIBLE);

        Picasso.with(context)
                .load(imageWithTitle.getImageUrl())
                .into(holder.image, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.imageProgress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        holder.imageProgress.setVisibility(View.GONE);
                    }
                });

        View.OnClickListener onClicked = (v) -> {
            Pair[] transitionViews = {
                    new Pair(holder.image, "image_transition"),
                    new Pair(holder.title, "text_transition")};
            onElementClicked.call(transitionViews, imageWithTitle.getImageUrl(),
                    imageWithTitle.getTitle());
        };

        holder.itemView.setOnClickListener(onClicked);
        holder.image.setOnClickListener(onClicked);
        holder.title.setOnClickListener(onClicked);
    }

    @Override
    public int getItemCount() {
        return elements.size();
    }

    public List<ImageWithTitle> getElements() {
        return elements;
    }

    class ImageWithTitleViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView title;
        ProgressBar imageProgress;

        public ImageWithTitleViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.image);
            title = (TextView) itemView.findViewById(R.id.text);
            imageProgress = (ProgressBar) itemView.findViewById(R.id.imageProgress);
        }
    }
}
