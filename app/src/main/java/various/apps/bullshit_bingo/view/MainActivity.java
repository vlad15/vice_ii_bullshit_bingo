package various.apps.bullshit_bingo.view;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;


import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import various.apps.base.presenter.Presenter;
import various.apps.base.view.BaseButterKnifeViewActivity;
import various.apps.base.view.DataReceiver;
import various.apps.bullshit_bingo.di.AppInjectorHolder;
import various.apps.bullshit_bingo.R;
import various.apps.bullshit_bingo.model.domain.ImageWithTitle;
import various.apps.bullshit_bingo.model.domain.PagingPosition;
import various.apps.bullshit_bingo.presenter.MainPresenter;
import various.apps.bullshit_bingo.view.adapters.ImageWithTitleAdapter;
import various.apps.bullshit_bingo.view.utils.StaggeredListElementsDecorator;

public class MainActivity extends BaseButterKnifeViewActivity implements MainView {

    private final String TAG = this.getClass().getSimpleName();

    private final static int COLUMNS_PORTRAIT = 3;
    private final static int COLUMNS_LANDSCAPE = 6;
    private final static int ITEMS_PER_PAGE = COLUMNS_PORTRAIT * 4;

    private int pagesLoaded = 0;
    private int lastDisplayedPage = -1;

    @Inject
    MainPresenter presenter;

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.images_with_titles)
    RecyclerView imagesWithTitles;

    @BindView(R.id.bottomProgress)
    ProgressBar bottomProgressBar;

    @BindView(R.id.rightProgress)
    ProgressBar rightProgressBar;

    @BindView(R.id.pageIndicator)
    TextView pageIndicator;

    @BindString(R.string.page)
    String PAGE_TEXT;

    ImageWithTitleAdapter imageWithTitleAdapter;
    private StaggeredGridLayoutManager staggeredLayoutManager;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void obtainPresenter() {
        ((AppInjectorHolder) getApplication()).getAppInjector().inject(this);
    }

    @Override
    protected Presenter getPresenter() {
        return presenter;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void setupViews() {
        super.setupViews();

        swipeRefreshLayout.setOnRefreshListener(() -> presenter.onSwipedToRefresh());

        imagesWithTitles.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
            }
        });

        imageWithTitleAdapter = new ImageWithTitleAdapter(this,
                (views, url, title)-> goToDetails(views, url, title));


        staggeredLayoutManager = new StaggeredGridLayoutManager(COLUMNS_PORTRAIT,
                StaggeredGridLayoutManager.VERTICAL);

        linearLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false);

        int marginBetweenListElements = getResources().getDimensionPixelSize(
                R.dimen.staggered_list_items_margin);

        imagesWithTitles.addItemDecoration(new StaggeredListElementsDecorator(
                marginBetweenListElements,
                marginBetweenListElements));

        imagesWithTitles.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                processVisibleElementsEvents();
            }
        });

        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            imagesWithTitles.setLayoutManager(staggeredLayoutManager);
        } else {
            imagesWithTitles.setLayoutManager(linearLayoutManager);
        }

        imagesWithTitles.setAdapter(imageWithTitleAdapter);

    }

    private void goToDetails(Pair<View, String>[] transitionViews,
                             String url,
                             String title) {
        Intent intent = new Intent(this, DetailsActivity.class);

        // Pass data object in the bundle and populate details activity.
        intent.putExtra(DetailsActivity.IMAGE_URL, url);
        intent.putExtra(DetailsActivity.IMAGE_TITLE, title);

        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(this, transitionViews);
        startActivity(intent, options.toBundle());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter.onOpenForFirstTime();
    }

    @Override
    public boolean isToolbarEnabled() {
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void showNextProgress() {
        if (imagesWithTitles.getLayoutManager().equals(staggeredLayoutManager)) {
            bottomProgressBar.setVisibility(View.VISIBLE);
        } else {
            rightProgressBar.setVisibility(View.VISIBLE);
        }
    }

    private void hideNextProgress() {
        if (imagesWithTitles.getLayoutManager().equals(staggeredLayoutManager)) {
            bottomProgressBar.setVisibility(View.GONE);
        } else {
            rightProgressBar.setVisibility(View.GONE);
        }
    }

    DataReceiver<List<ImageWithTitle>, PagingPosition>
            imagesWithTitlesReceiver =
            new DataReceiver<List<ImageWithTitle>, PagingPosition>() {

        @Override
        public void onDataLoading(PagingPosition pagingPosition) {
            if (pagingPosition == null || pagingPosition.getPage() == 0) {
                swipeRefreshLayout.setRefreshing(true);
                hideNextProgress();
            } else {
                showNextProgress();
            }
        }

        @Override
        public void onDataReceived(List<ImageWithTitle> imagesWithTitle, PagingPosition pagingPosition) {

            pagesLoaded = pagingPosition.getPage() + 1;

            if (pagingPosition.getPage() == 0) {
                imageWithTitleAdapter.getElements().clear();
                imageWithTitleAdapter.getElements().addAll(imagesWithTitle);
                imageWithTitleAdapter.notifyDataSetChanged();
            } else {
                int wasElements = imageWithTitleAdapter.getItemCount();
                imageWithTitleAdapter.getElements().addAll(imagesWithTitle);
                imageWithTitleAdapter.notifyItemRangeInserted(wasElements,
                        imagesWithTitle.size());
            }

            swipeRefreshLayout.setRefreshing(false);
            hideNextProgress();

            imagesWithTitles.post(() -> {
                processVisibleElementsEvents();
            });

        }

        @Override
        public void onErrorReceived(Throwable error, PagingPosition pagingPosition) {
            swipeRefreshLayout.setRefreshing(false);
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                    .setMessage(error.getMessage())
                    .create();
            alertDialog.show();
        }
    };

    private void processVisibleElementsEvents() {
        int[] visibleItems;

        if (imagesWithTitles.getLayoutManager().equals(staggeredLayoutManager)) {
            visibleItems = staggeredLayoutManager.findLastVisibleItemPositions(null);
        } else {
            visibleItems = new int[1];
            visibleItems[0] = linearLayoutManager.findLastVisibleItemPosition();
        }

        if (isLastElementVisible(visibleItems)) {
            presenter.onLastElementVisible();
        }

        int[] firstCompletelyVisibleItems;
        if (imagesWithTitles.getLayoutManager().equals(staggeredLayoutManager)) {
            firstCompletelyVisibleItems =
                    staggeredLayoutManager.findFirstCompletelyVisibleItemPositions(null);
        } else {
            firstCompletelyVisibleItems = new int[1];
            firstCompletelyVisibleItems[0] =
                    linearLayoutManager.findFirstVisibleItemPosition();
        }
        displayCurrentPage(getCurrentVisiblePage(firstCompletelyVisibleItems));
    }

    private boolean isLastElementVisible(int [] visibleItems) {

        for (int visibleItemNumber: visibleItems) {
            if (visibleItemNumber == imageWithTitleAdapter.getItemCount()-1)
                return true;
        }

        return false;
    }

    private int getCurrentVisiblePage(int[] elements) {
        int minimalElement = Integer.MAX_VALUE;
        for (int element: elements) {
            minimalElement = Math.min(minimalElement, element);
        }

        return minimalElement / ITEMS_PER_PAGE;
    }

    private void displayCurrentPage(int pageNumber) {
        if (lastDisplayedPage != pageNumber) {
            lastDisplayedPage = pageNumber;

            pageIndicator.setText(PAGE_TEXT + (pageNumber + 1));
        }
    }

    public DataReceiver<List<ImageWithTitle>, PagingPosition> getImagesWithTitlesReceiver() {
        return imagesWithTitlesReceiver;
    }

    @Override
    public int getItemsPerPage() {
        return ITEMS_PER_PAGE;
    }

    @Override
    public int getPagesLoad() {
        return pagesLoaded;
    }
}
