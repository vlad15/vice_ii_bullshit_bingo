package various.apps.bullshit_bingo.view;

import java.util.List;

import various.apps.base.view.DataReceiver;
import various.apps.base.view.View;
import various.apps.bullshit_bingo.model.domain.ImageWithTitle;
import various.apps.bullshit_bingo.model.domain.PagingPosition;

public interface MainView extends View {

    DataReceiver<List<ImageWithTitle>, PagingPosition>
        getImagesWithTitlesReceiver();

    int getItemsPerPage();
    int getPagesLoad();
}
