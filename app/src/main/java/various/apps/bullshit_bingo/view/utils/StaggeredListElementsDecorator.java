package various.apps.bullshit_bingo.view.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class StaggeredListElementsDecorator extends RecyclerView.ItemDecoration {
    int horizontalMargin;
    int verticalMargin;

    public StaggeredListElementsDecorator(int horizontalMargin, int verticalMargin) {
        this.horizontalMargin = horizontalMargin;
        this.verticalMargin = verticalMargin;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        outRect.left = horizontalMargin;
        outRect.right = horizontalMargin;
        outRect.bottom = verticalMargin;
        outRect.top = verticalMargin;
    }
}
