package various.apps.bullshit_bingo.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import various.apps.base.presenter.Presenter;
import various.apps.base.view.BaseButterKnifeViewActivity;
import various.apps.bullshit_bingo.di.AppInjectorHolder;
import various.apps.bullshit_bingo.R;
import various.apps.bullshit_bingo.presenter.DetailsPresenter;

public class DetailsActivity extends BaseButterKnifeViewActivity implements DetailsView {

    public static final String IMAGE_URL = "IMAGE_URL";
    public static final String IMAGE_TITLE = "IMAGE_TITLE";

    String imageUrl;
    String title;

    RotateAnimation rotateAnimation;

    @Inject
    DetailsPresenter presenter;

    @BindView(R.id.image)
    ImageView imageView;

    @BindView(R.id.text)
    TextView textView;

    @BindView(R.id.imageProgress)
    ProgressBar imageProgress;

    @BindView(R.id.wrapperAroundRotatedContent)
    View viewToRotate;

    @Override
    protected void obtainPresenter() {
        ((AppInjectorHolder) getApplication()).getAppInjector().inject(this);
    }

    @Override
    protected Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        Intent intent = getIntent();
        imageUrl = intent.getStringExtra(IMAGE_URL);
        title = intent.getStringExtra(IMAGE_TITLE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.favorite_button:
                rotateView(viewToRotate);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void rotateView(View view) {

        if (rotateAnimation != null) {
            rotateAnimation.cancel();
        }

        rotateAnimation = new RotateAnimation(0f, 720f,
                view.getWidth() / 2, view.getHeight() / 2);
        rotateAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        rotateAnimation.setDuration(4000);

        view.startAnimation(rotateAnimation);

    }

    @Override
    protected void setupViews() {
        super.setupViews();

        textView.setText(title);

        imageProgress.setVisibility(View.VISIBLE);

        Picasso.with(this)
                .load(imageUrl)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        imageProgress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        imageProgress.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.details_menu, menu);
        return true;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_details;
    }

    @Override
    public boolean isToolbarEnabled() {
        return true;
    }
}
