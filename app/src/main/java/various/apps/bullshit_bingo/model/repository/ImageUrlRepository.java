package various.apps.bullshit_bingo.model.repository;

import java.util.List;

import rx.Observable;

public interface ImageUrlRepository {
    Observable<List<String>> get(int page, int perPage);
}
