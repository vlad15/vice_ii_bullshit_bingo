package various.apps.bullshit_bingo.model.interactor;

import java.util.List;

import rx.Observable;
import various.apps.bullshit_bingo.model.domain.ImageWithTitle;

public interface GetImagesWithTitlesInteractor {
    Observable<List<ImageWithTitle>> get(int page, int perPage);
}
