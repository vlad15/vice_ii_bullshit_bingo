package various.apps.bullshit_bingo.model.domain;

public class PagingPosition {
    private int page;
    private int itemsPerPage;

    public PagingPosition(int page, int itemsPerPage) {
        this.page = page;
        this.itemsPerPage = itemsPerPage;
    }

    public int getPage() {
        return page;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }
}
