package various.apps.bullshit_bingo.model.repository;

import java.util.List;

import rx.Observable;

public interface RandomWordsRepository {
    Observable<String> get();
    Observable<List<String>> get(int quantity);
}
