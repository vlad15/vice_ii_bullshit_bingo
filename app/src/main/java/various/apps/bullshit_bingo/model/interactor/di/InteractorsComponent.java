package various.apps.bullshit_bingo.model.interactor.di;

import dagger.Component;
import various.apps.bullshit_bingo.di.Scopes.PresenterSoftScope;
import various.apps.bullshit_bingo.model.interactor.GetImagesWithTitlesInteractor;
import various.apps.bullshit_bingo.model.repository.di.RepositoriesComponent;

@PresenterSoftScope
@Component(
        dependencies = {RepositoriesComponent.class},
        modules = {InteractorsModule.class})
public interface InteractorsComponent {
    GetImagesWithTitlesInteractor getImagesWithTitlesInteractor();
}
