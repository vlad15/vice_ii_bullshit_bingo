package various.apps.bullshit_bingo.model.domain;

public class ImageWithTitle {
    private String imageUrl;
    private String title;

    public ImageWithTitle(String imageUrl, String title) {
        this.imageUrl = imageUrl;
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getTitle() {
        return title;
    }
}
