package various.apps.bullshit_bingo.model.interactor;

import java.util.List;

import rx.Observable;
import various.apps.bullshit_bingo.model.domain.ImageWithTitle;
import various.apps.bullshit_bingo.model.repository.ImageUrlRepository;
import various.apps.bullshit_bingo.model.repository.RandomWordsRepository;

public class GetImagesWithTitlesMurrayInteractor implements GetImagesWithTitlesInteractor {

    ImageUrlRepository imageUrlRepository;
    RandomWordsRepository randomWordsRepository;

    public GetImagesWithTitlesMurrayInteractor(
            ImageUrlRepository imageUrlRepository,
            RandomWordsRepository randomWordsRepository) {

        this.imageUrlRepository = imageUrlRepository;
        this.randomWordsRepository = randomWordsRepository;
    }

    @Override
    public Observable<List<ImageWithTitle>> get(int page, int perPage) {
        return imageUrlRepository.get(page, perPage)
                .concatMap(urls ->
                        Observable.zip(
                                Observable.from(urls),
                                randomWordsRepository.get(urls.size())
                                        .concatMap(Observable::from),
                                (imageUrl, imageTitle) ->
                                        new ImageWithTitle(
                                                imageUrl,
                                                imageTitle)
                        ))
                .toList();
    }
}
