package various.apps.bullshit_bingo.model.interactor.di;

import dagger.Module;
import dagger.Provides;
import various.apps.bullshit_bingo.di.Scopes.PresenterSoftScope;
import various.apps.bullshit_bingo.model.interactor.GetImagesWithTitlesInteractor;
import various.apps.bullshit_bingo.model.interactor.GetImagesWithTitlesMurrayInteractor;
import various.apps.bullshit_bingo.model.interactor.utils.SoftCache;
import various.apps.bullshit_bingo.model.repository.ImageUrlRepository;
import various.apps.bullshit_bingo.model.repository.RandomWordsRepository;

@Module
public class InteractorsModule {

    SoftCache softCache = new SoftCache();

    @Provides
    @PresenterSoftScope
    public GetImagesWithTitlesInteractor provideGetImagesWithTitlesInteractor(
            ImageUrlRepository imageUrlRepository,
            RandomWordsRepository randomWordsRepository
    ) {

        return softCache.getCachedOrCreate(
                GetImagesWithTitlesInteractor.class,
                () ->
                        new GetImagesWithTitlesMurrayInteractor(
                            imageUrlRepository, randomWordsRepository));
    }
}
