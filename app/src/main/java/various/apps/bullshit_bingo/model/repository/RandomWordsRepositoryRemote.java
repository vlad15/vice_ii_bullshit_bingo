package various.apps.bullshit_bingo.model.repository;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.GET;
import rx.Observable;
import various.apps.bullshit_bingo.BuildConfig;

public class RandomWordsRepositoryRemote implements RandomWordsRepository {

    private Endpoints endpoints;

    interface Endpoints {
        @GET("get.php")
        Observable<String> getWord();
    }

    private Retrofit getRetrofitPlainText() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            builder = builder.addInterceptor(interceptor);
        }

        OkHttpClient client = builder
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.setgetgo.com/randomword/")
                .client(client)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        return retrofit;
    }

    public RandomWordsRepositoryRemote() {

        Retrofit retrofitPlainText = getRetrofitPlainText();

        retrofitPlainText = getRetrofitPlainText();
        endpoints = retrofitPlainText.create(Endpoints.class);

    }

    @Override
    public Observable<String> get() {
        return endpoints.getWord();
    }

    @Override
    public Observable<List<String>> get(int quantity) {
        List<Observable<String>> observables = new ArrayList<>();

        for (int i = 0; i < quantity; ++i) {
            observables.add(get());
        }

        return Observable.zip(observables, (wordsObjects) -> {

                    List<String> words = new ArrayList<>();

                    for (Object wordObject : wordsObjects) {
                        words.add((String) wordObject);
                    }

                    return words;
                }
        );
    }


}
