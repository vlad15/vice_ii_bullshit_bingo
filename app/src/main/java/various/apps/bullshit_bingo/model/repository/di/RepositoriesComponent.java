package various.apps.bullshit_bingo.model.repository.di;

import dagger.Component;
import various.apps.bullshit_bingo.di.Scopes.InteractorSoftScope;
import various.apps.bullshit_bingo.model.repository.ImageUrlRepository;
import various.apps.bullshit_bingo.model.repository.RandomWordsRepository;

@InteractorSoftScope
@Component(modules = {
        RepositoriesModule.class
})
public interface RepositoriesComponent {
    ImageUrlRepository imageUrlRepository();
    RandomWordsRepository randomWordsRepository();
}
