package various.apps.bullshit_bingo.model.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import rx.Observable;

public class ImageUrlRepositoryMurray implements ImageUrlRepository {

    List<String> URLS = Collections.unmodifiableList(Arrays.asList(
            "https://www.fillmurray.com/460/300",
            "https://www.fillmurray.com/300/200",
            "https://www.fillmurray.com/140/200",
            "https://www.fillmurray.com/g/155/300",
            "https://www.fillmurray.com/140/100",
            "https://www.fillmurray.com/g/140/100",
            "https://www.fillmurray.com/284/196"
    ));

    @Override
    public Observable<List<String>> get(int page, int perPage) {

        return Observable.just(null)
                .map(aVoid -> {
                    List<String> urls = new ArrayList<>();

                    for (int i = 0; i < perPage; ++i) {
                        urls.add(URLS.get(
                                (i + page * perPage) % URLS.size())
                        );
                    }

                    return urls;
                });
    }
}
