package various.apps.bullshit_bingo.model.repository.di;

import dagger.Module;
import dagger.Provides;
import various.apps.bullshit_bingo.di.Scopes.InteractorSoftScope;
import various.apps.bullshit_bingo.model.interactor.utils.SoftCache;
import various.apps.bullshit_bingo.model.repository.ImageUrlRepository;
import various.apps.bullshit_bingo.model.repository.ImageUrlRepositoryMurray;
import various.apps.bullshit_bingo.model.repository.RandomWordsRepository;
import various.apps.bullshit_bingo.model.repository.RandomWordsRepositoryRemote;


@Module
public class RepositoriesModule {

    SoftCache softCache = new SoftCache();

    @Provides
    @InteractorSoftScope
    public ImageUrlRepository provideImageUrlRepository() {
        return softCache.getCachedOrCreate(
                ImageUrlRepository.class,
                () ->
                    new ImageUrlRepositoryMurray()
        );
    }

    @Provides
    @InteractorSoftScope
    public RandomWordsRepository provideRandomWordsRepository() {
        return softCache.getCachedOrCreate(
                RandomWordsRepository.class,
                () ->
                    new RandomWordsRepositoryRemote());
    }
}
