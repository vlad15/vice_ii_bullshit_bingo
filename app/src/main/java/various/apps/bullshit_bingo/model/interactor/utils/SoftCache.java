package various.apps.bullshit_bingo.model.interactor.utils;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

import rx.functions.Func0;

public class SoftCache {

    private final Map<Class, SoftReference> cache = new HashMap<>();

    public synchronized <Data> Data
            getCachedOrCreate(Class<Data> dataClass, Func0<Data> createObject) {
        Data object = null;

        if (cache.containsKey(dataClass)) {
            // Data is assigned to class safely
            //noinspection unchecked
            object = (Data) cache.get(dataClass).get();
        }

        if (object == null) {
            object = createObject.call();
            cache.put(dataClass, new SoftReference<>(object));
        }

        return object;
    }
}
