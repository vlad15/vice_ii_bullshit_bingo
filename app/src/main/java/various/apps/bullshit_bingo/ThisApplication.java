package various.apps.bullshit_bingo;

import android.app.Application;
import android.content.Context;

import com.squareup.leakcanary.LeakCanary;

import various.apps.bullshit_bingo.di.AppComponent;
import various.apps.bullshit_bingo.di.AppInjector;
import various.apps.bullshit_bingo.di.AppInjectorHolder;

import various.apps.bullshit_bingo.di.AppModule;
import various.apps.bullshit_bingo.di.DaggerAppComponent;
import various.apps.bullshit_bingo.model.interactor.di.DaggerInteractorsComponent;
import various.apps.bullshit_bingo.model.interactor.di.InteractorsComponent;
import various.apps.bullshit_bingo.model.interactor.utils.SoftCache;
import various.apps.bullshit_bingo.model.repository.di.DaggerRepositoriesComponent;
import various.apps.bullshit_bingo.model.repository.di.RepositoriesComponent;
import various.apps.bullshit_bingo.presenter.di.DaggerPresentersComponent;
import various.apps.bullshit_bingo.presenter.di.PresentersComponent;

public class ThisApplication extends Application implements AppInjectorHolder {

    private AppComponent appComponent;
    private final Object appComponentMonitor = new Object();

    private SoftCache softCache = new SoftCache();

    @Override
    public AppInjector getAppInjector() {
        return getPresentersComponent(this);
    }

    private static AppComponent getAppComponent(Context context) {
        ThisApplication application = (ThisApplication) context.getApplicationContext();
        if (application.appComponent == null) {
            synchronized (application.appComponentMonitor) {
                if (application.appComponent == null) {

                    application.appComponent = DaggerAppComponent
                            .builder()
                            .appModule(new AppModule(context))
                            .build();

                }
            }

        }

        return application.appComponent;
    }

    private static PresentersComponent getPresentersComponent(Context context) {
        ThisApplication application = (ThisApplication) context.getApplicationContext();

        RepositoriesComponent repositoriesComponent =
                application.softCache.getCachedOrCreate(
                        RepositoriesComponent.class,
                        () ->
                            DaggerRepositoriesComponent
                            .builder()
                            .build()
                );

        InteractorsComponent interactorsComponent =
                application.softCache.getCachedOrCreate(
                        InteractorsComponent.class,
                        () ->
                                DaggerInteractorsComponent
                                .builder()
                                .repositoriesComponent(repositoriesComponent)
                                .build()
                );

        return application.softCache.getCachedOrCreate(
                PresentersComponent.class,
                () ->
                    DaggerPresentersComponent
                            .builder()
                            .appComponent(getAppComponent(application))
                            .interactorsComponent(interactorsComponent)
                            .build()
        );

    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            if (LeakCanary.isInAnalyzerProcess(this)) {
                // This process is dedicated to LeakCanary for heap analysis.
                // You should not init your app in this process.
                return;
            }
            LeakCanary.install(this);
        }
    }

}
